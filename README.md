# Treasure-finder

> A little script for finding 'free' games on the interwebs written in [elvish](https://github.com/elves/elvish)

Basicaly it just pulls relevant data from /r/GameDeals
and displays it in your terminal

## TODO
* [ ] Make it run as daemon. With regular notifications.
* [ ] Custom notification command support.

## Usage

```
Usage: ./treasure-finder.elv [args] 
-h | --help      show help
-s | --sort <new|hot>    results sorting (default: hot)
-d               daemonize (NYI)
-t <time>        refetch interval when running as daemon (default: 1800 sec) (NYI)
-n <cmd>         command for sending notification (NYI)
-a | --user-agent <str>  specify custom user-agent
```

## Installation

1. Install dependencies listed below
2. 
```
  git clone https://gitlab.com/SneakyThunder/treasure-finder.git`
  cd treasure-finder
  chmod +x treasure-finder.elv
  ./treasure-finder.elv
```

## Dependencies

* curl (Should be installed by default on most \*nix systems)
* [elvish](https://github.com/elves/elvish)

#### P.S
  If you have a better name for this project,
  please open an issue to share it.

