#!/usr/bin/env elvish
# Script for finding games with 100% discount
# on the Interwebs - by sneaky
#
# Deps: elvish curl

#   This program is free software. It comes without any warranty, to
#   the extent permitted by applicable law. You can redistribute it
#   and/or modify it under the terms of the Do What The Fuck You Want
#   To Public License, Version 2, as published by Sam Hocevar. See
#   http://www.wtfpl.net/ for more details.


use flag;
use str;

var interval;
var notify-cmd;
var agent = "Treasure finder (elvish)";
var url-opt = "hot"

var specs = [
     [&short=h &long=help]
     [&short=a &long=user-agent &arg-required]
     [&short=s &long=sort &arg-required]
     [&short=d]
     [&short=t]
     [&short=n]
   ]

fn add {|list x|
  put [$list[(range (count $list))] $x]
}

fn show-help {
  echo "Usage: "
  echo "-h | --help\t show help"
  echo "-s | --sort <new|hot>\t results sorting (default: hot)"
  echo "-d\t\t daemonize (NYI)"
  echo "-t <time>\t refetch interval when running as daemon (default: 1800 sec) (NYI)"
  echo "-n <cmd>\t command for sending notification (NYI)"
  echo "-a | --user-agent <str>\t specify custom user-agent"
}

fn setup-vars {|opts|
  peach {|x|
    var p = $x[spec][short]
    if (eq $p h) { 
      show-help
      exit 0
    } elif (eq $p a) { 
      set agent = $x[arg] 
    } elif (or (eq $p d) (eq $p t) (eq $p n)) {
      echo Sorry, -$p is NYI!
      exit 1;
    } elif (eq $p s) {
        if (and (not-eq $x[arg] hot) (not-eq $x[arg] new)) {
          echo "Invalid argument: "$x[arg]" for -"$p
          exit 1
        } else {
          set url-opt = $x[arg]
        }
    }
  } $opts
}

fn filter-data {|data &tr=100|
  var res = []
  each {|x|
    if (or (str:contains $x[data][title] "100%") (str:contains (str:to-lower $x[data][title]) "free")) {
      set res = (add $res [&title=$x[data][title] &link=$x[data][url] &date=(e:date --date=@$x[data][created] +%d/%m)])
    }
  } $data
  put $res
}

fn parse-n-filter {|x|
  if (has-key $x error) {
    echo "Cannot retrive data. Error "$x[error]
    exit 4
  } else {
    filter-data $x[data][children]
  }
}

fn print-result {|list|
  each {|x|
    print (styled $x[date] green)" | "(styled $x[title] cyan bold)" ("(styled $x[link] blue dim)")\n"
  } $list
}

flag:parse-getopt $args $specs | setup-vars [(all)][0]

e:curl -A $agent "https://www.reddit.com/r/gamedeals/"$url-opt"/.json" | from-json | parse-n-filter (all)  | print-result (all)
